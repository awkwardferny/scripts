#!/bin/bash

while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
    -u|--username)
      USERNAME="$2"
      shift
      shift
      ;;
  esac
done

if [ -z ${USERNAME} ]
then
  echo "Must Enter a Username"
  exit 0
fi

git config --global user.name "Fernando Diaz"

if [ "${USERNAME}" == "awkwardferny" ]
then
  git config user.email "awkwardferny@gmail.com"
elif [ "${USERNAME}" == "fjdiaz" ]
then
  git config user.email "fdiaz@gitlab.com"
elif [ "${USERNAME}" == "diazjf" ]
then
  git config user.email "awkwardferny@gmail.com"
else
  echo "Please enter a Valid Username"
fi

echo "Username set to ${USERNAME}"
exit 0
