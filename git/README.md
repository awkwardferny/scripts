# Git Configuration for my Repos

These settings are used for setting up my git repository on a new machine.

## Functions

1. Generate private SSH keys

This Function will Create 3 SSH-Keys:
- 
- 
- 

2. Move config file to proper location

This function moves the config containing which
emails to use for cloning and pushing code. When cloning
you must use the correct username:

```
$ git clone git@<username>
```

Where <username> can be:

- 
- 
-

as found in the config file.

3. Add change-user script to /bin

This function adds a new script which let's use change our
Git User on Demand by running the following command:

```
$ switch-git -u awkwardferny
```
