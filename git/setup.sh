#!/bin/bash

# generate ssh keys, if non-existent
if [[ ! -f "~/.ssh/gitlab-work" ]]; then
    ssh-keygen -q -t rsa -N '' -f ~/.ssh/gitlab-work
fi

if [[ ! -f "~/.ssh/gitlab-personal" ]]; then
    ssh-keygen -q -t rsa -N '' -f ~/.ssh/gitlab-personal
fi

if [[ ! -f "~/.ssh/github-personal" ]]; then
    ssh-keygen -q -t rsa -N '' -f ~/.ssh/github-personal
fi

# move config to ~/.ssh
sudo cp config ~/.ssh/config

# move switch_user script to be used as alias command
sudo cp -f switch_git.sh /usr/local/bin/switch-git

sudo chown -R $(whoami) /usr/local/bin/switch-git
