#!/bin/bash

# Install vim
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  sudo apt-get update
  sudo apt-get install vim
elif [[ "$OSTYPE" == "darwin"* ]]; then
  brew update
  brew install vim
else
  echo "Cannot Detect OS"
  exit 1
fi

# Create Directory to house vim packages
mkdir -p ~/.vim/pack/vendor/start

# Start adding plugins
git clone --depth 1 \
https://github.com/preservim/nerdtree.git \
~/.vim/pack/vendor/start/nerdtree

git clone --depth 1 \
https://github.com/vim-airline/vim-airline.git \
~/.vim/pack/my-plugins/start/airline

git clone --depth 1 \
https://github.com/fatih/vim-go.git \
~/.vim/pack/plugins/start/vim-go

# Copy over vim-rc
cp -f .vimrc ~/.vimrc
