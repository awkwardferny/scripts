# Vim Configuration

These settings are used for setting up vim
on a new machine.

## Usage

### Python Auto-Complete
```

```

### GoLang Auto-Complete

```

```


### Enable NERDTree

```
Ctrl + g
```

## Plugins

- Airline
- NerdTree
- Vim-Go
