# Gists

This contains some useful commands the I use commonly and wanna remember.

## Deleting Kubernetes Pods

```
$ kubectl delete pods $(awk '{print $2}')
```
